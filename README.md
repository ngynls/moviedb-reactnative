# React Native Movie Watchlist App
A mobile app allowing users to add, update & delete movies from their watchlist stored via cloud using Google's Firebase Realtime Database.

## Technologies used:
* Javascript (ES6)
* React Native
* NodeJS (npm)
* Google Firebase

## How to run
* Make sure Expo CLI is installed in your local machine
* Clone the repository & cd into it https://bitbucket.org/ngynls/moviedb-reactnative.git
* `expo start`
