import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MovieSearchResult from '../components/MovieSearchResult';
import SearchHeader from 'react-native-search-header';
import BottomBar from '../components/BottomBar';

export default class SearchScreen extends React.Component {

  static navigationOptions={
    header:null
  }

  constructor(props){
    super(props);

    this.state={
      visible:false,
    };
  }

  makeRequestToAPI=async(str)=>{
    const key=require('../config/apiKey').API_KEY;
    const baseUri="https://api.themoviedb.org/3/search/movie?api_key=";
    let query=str.replace(/\s/g, '+');
    const response=await fetch(baseUri+key+"&query="+query)
    .then((res)=>res.json())
    .catch((err)=>console.log(err))
    .finally((res)=> this.props.navigation.navigate('SearchResults', {
      results: res.results
    }));
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Text style={styles.topHeader}>Browse movies</Text>
            <TouchableOpacity onPress={()=>this.searchHeader.show()}>
              <Icon name='search' size={30} style={styles.searchIcon}/>
            </TouchableOpacity>
          <SearchHeader
                      ref = {(searchHeader) => {
                          this.searchHeader = searchHeader;
                      }}
                      onClear = {() => {
                          console.log(`Clearing input!`);
                      }}
                      onSearch={
                        (event) => {
                        this.makeRequestToAPI(event.nativeEvent.text);
                      }
                    }
                  />
        </View>
        <View style={styles.body}>
          <Icon name='search' size={40}/>
          <Text>Please enter your search</Text>
        </View>
        <BottomBar navigation={this.props.navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navBar:{
    height:70,
    backgroundColor:"white",
    elevation:3,
    paddingTop:24,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  body:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  topHeader:{
    fontSize:20,
    paddingLeft:20,
    paddingTop:10,
  },
  searchIcon:{
    paddingTop:10,
    paddingRight:10,
  },
});
