import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Platform, ToastAndroid, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import TopBar from '../components/TopBar';
import BottomBar from '../components/BottomBar';
const firebase=require('firebase');
const sha256 = require('js-sha256');

export default class SettingsScreen extends React.Component {

  static navigationOptions={
    header:null
  }

  constructor(props){
    super(props);

    this.state={
      username: '',
      memberSince: '',
      loading: true,
    };
  }

  componentDidMount(){
    const username=firebase.auth().currentUser.email;
    const memberSinceRef= firebase.database().ref(`users/${sha256(username)}/memberSince`);
    memberSinceRef.on('value', (snapshot)=> this.setState({username, memberSince:snapshot.val()}));
    setTimeout(()=>{
      this.setState({
        loading: false
      })
    },1000);
  }

  logout=()=>{
  firebase.auth().signOut().then(()=>{
    if(Platform.OS==='android')
    ToastAndroid.show('User is logged out!', ToastAndroid.LONG);
    this.redirectToLoginScreen();
  });
}

  redirectToLoginScreen=()=>this.props.navigation.navigate('Login');

  render(){
    return (
      <View style={styles.container}>
        <TopBar header="Settings" />
        <View style={styles.body}>
          {
            this.state.loading ?
            <ActivityIndicator size="large" color="#0000ff" />
            :
            <View style={styles.body}>
              <Text>Username: {this.state.username}</Text>
              <Text>Member since: {this.state.memberSince}</Text>
              <TouchableOpacity style={styles.logoutButton} onPress={()=>this.logout()}>
                  <Text style={{color:'white'}}>Logout</Text>
              </TouchableOpacity>
            </View>
          }
        </View>
        <BottomBar navigation={this.props.navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  body:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  logoutButton:{
    width:70,
    height:40,
    padding:10,
    marginTop:15,
    backgroundColor: 'red',
    borderRadius:5,
    justifyContent:'center'
  }
});
