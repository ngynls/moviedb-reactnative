import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MovieSearchResult from '../components/MovieSearchResult';
import TopBar from '../components/TopBar';
import BottomBar from '../components/BottomBar';

const firebase=require('firebase');
const sha256 = require('js-sha256');

export default class SearchResultsScreen extends React.Component {

  static navigationOptions={
    header:null
  }

  constructor(props){
    super(props);

    this.state={
      loading: true,
      data:[],
      addedTitles:new Set()
    };
  }

  componentDidMount(){
    this.username=firebase.auth().currentUser.email;
    this.movieListRef=firebase.database().ref(`users/${sha256(this.username)}/movieList`);
    const results = this.props.navigation.getParam('results', 'NO-ID');
    this.setState({data: results});
    setTimeout(()=>{
      this.setState({
        loading: false
      })
    },1000);
  }

  retrieveTitlesFromList=()=>{
    let listedTitles=new Set([]);
    this.movieListRef.once('value', (snapshot)=>{
      snapshot.forEach((data)=>{
        listedTitles.add(data.val().moviedbID);
      });
      this.setState({addedTitles: listedTitles});
    });
  }

  displayMovies=()=>{
      const titles=this.retrieveTitlesFromList();
      return this.state.data.map((x,i)=>
        this.state.addedTitles.has(x.id) ?
        (<MovieSearchResult key={i} movieInfo={x} status="Added" />) :
        (<MovieSearchResult key={i} movieInfo={x} status="Not added" />)
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <TopBar header="Search Results" />
          {
            this.state.loading ?
            <View style={styles.activityInd}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
            :
            <ScrollView style={styles.body}>
              {this.displayMovies()}
            </ScrollView>
          }
          <BottomBar navigation={this.props.navigation} />
      </View>
    );
  }

  componentWillUnmount(){
    this.movieListRef.off();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  activityInd:{
    flex:1,
    justifyContent: 'center',
  },
  body:{
    flex:1
  },
});
