import React from 'react';
import { StyleSheet, Text, View, StatusBar, TouchableOpacity } from 'react-native';
import Form from '../components/Form';

export default class LoginScreen extends React.Component {

  static navigationOptions={
    header:null
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar style={{backgroundColor:'#aeaeae' , barStyle:'light-content'}} />
        <Form role="Login" navigation={this.props.navigation}/>
        <TouchableOpacity style={{marginTop:150, paddingBottom:15}}
          onPress={()=>this.props.navigation.navigate('Signup')}>
        <Text style={{color:'#000000'}}> Not a member yet? Sign up now!</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e0e0e0',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
