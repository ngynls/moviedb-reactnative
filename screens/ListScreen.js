import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Platform, ToastAndroid, Alert, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MovieListElement from '../components/MovieListElement';
import MovieListElementWithoutButtons from '../components/MovieListElementWithoutButtons';
import TopBar from '../components/TopBar';
import BottomBar from '../components/BottomBar';
import { ActionPicker } from 'react-native-action-picker';
import SegmentedControlTab from 'react-native-segmented-control-tab';

const firebase=require('firebase');
const sha256 = require('js-sha256');

export default class ListScreen extends React.Component {

  static navigationOptions={
    header:null
  }

  constructor(props){
    super(props);

    this.state={
      movieList:[],
      filteredList:[],
      isModalVisible: false,
      filmToUpdate: [],
      selectedIndex: 0,
    };
  }

  componentDidMount(){
    console.log("Component is mounted");
    this.getMovieList();
  }

  handleIndexChange = index => {
  let filteredList;
  if (index === 1) {
    filteredList = this.state.movieList.filter(
      film => film.status === 'Completed'
    );
  }
  else if (index === 2) {
    filteredList = this.state.movieList.filter(
      film => film.status === 'Watching'
    );
  }
  else if (index === 3) {
    filteredList = this.state.movieList.filter(
      film => film.status === 'Plan to watch'
    );
  }
  this.setState({
    selectedIndex: index,
    filteredList,
  });
}

  getMovieList=()=>{
    const username=firebase.auth().currentUser.email;
    const movieListRef=firebase.database().ref(`users/${sha256(username)}/movieList`);
    movieListRef.once('value', (snapshot)=>{
      snapshot.forEach((data)=>{
        const movieObj={
          id: data.key,
          moviedbID: data.val().moviedbID,
          title: data.val().title,
          status: data.val().status,
        };
        this.setState({
          movieList:this.state.movieList.concat(movieObj)
        });
        //console.log(this.state.movieList);
      });
    });
  }

  updateFilm=(film, status)=>{
    //update status in the state
    let updatedList=[...this.state.movieList];
    let index= updatedList.indexOf(film);
    updatedList[index].status=status;
    this.setState({
      movieList:updatedList,
      isModalVisible: !this.state.isModalVisible,
    });
    //update status in firebase
    const username=firebase.auth().currentUser.email;
    const toUpdateRef=firebase.database().ref(`users/${sha256(username)}/movieList/${film.id}`);
    toUpdateRef.update({status});
  }

  deleteAlertBox= film =>{
    Alert.alert('Info', `Are you sure you want to delete [${film.title}] from your list?`,
    [
      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      {text: 'Yes', onPress: () => this.deleteFilm(film)},
    ]);
  }

  deleteFilm=film=>{
    //remove the film & update the state
    let updatedList=[...this.state.movieList];
    let index = updatedList.indexOf(film);
    updatedList.splice(index, 1);
    this.setState({
      movieList:updatedList
    });
    //delete from firebase
    const username=firebase.auth().currentUser.email;
    const toDeleteRef=firebase.database().ref(`users/${sha256(username)}/movieList/${film.id}`);
    toDeleteRef.remove();
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
  }

  createOptions = film => {
  return [
    {
      label: 'Plan to watch',
      action: () => this.updateFilm(film, 'Plan to watch'),
    },
    { label: 'Watching',
      action: () => this.updateFilm(film, 'Watching')
    },
    {
      label: 'Completed',
      action: () => this.updateFilm(film, 'Completed'),
    },
    ];
  }

  render(){
    return (
      <View style={styles.container}>
        <TopBar header="My list" />
          <View style={styles.filterTabStyle}>
            <SegmentedControlTab
              values={['All', 'Completed', 'Watching', 'Planned']}
              selectedIndex={this.state.selectedIndex}
              onTabPress={this.handleIndexChange}
              tabStyle={{
                borderColor: 'black',
              }}
              activeTabStyle={{ backgroundColor: 'white', marginTop: 2 }}
              tabTextStyle={{ color: '#444444', fontWeight: 'bold' }}
              activeTabTextStyle={{ color: '#888888' }}
              />
          </View>
        <View style={styles.body}>
          {
            this.state.selectedIndex === 0 ?
            (
              <FlatList
                  data={this.state.movieList}
                  keyExtractor={(x, i) => x.moviedbID.toString()}
                  renderItem={({ item, key }) => (
                    <MovieListElement
                      movieInfo={item}
                      deleteMethod={() => this.deleteAlertBox(item)}
                      updateMethod={() => {
                      (() => this.setState({ filmToUpdate: item }))();
                      (() => this.toggleModal())();
                      }}
                    />
                  )}
              />
          ) : (
            <FlatList
                data={this.state.filteredList}
                keyExtractor={(x, i) => x.moviedbID.toString()}
                renderItem={({ item, key }) => (
                  <MovieListElementWithoutButtons
                    movieInfo={item}
                  />
                )}
            />
          )
          }
          <ActionPicker
            options={this.createOptions(this.state.filmToUpdate)}
            isVisible={this.state.isModalVisible}
            onCancelRequest={this.toggleModal}
          />
        </View>
        <BottomBar navigation={this.props.navigation} />
      </View>
    );
  }

  componentWillUnmount(){
    console.log("Component will unmount");
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  body:{
    flex:1,
  },
  filterTabStyle: {
    paddingVertical: 15,
  },
  displayContainer:{
    flex:1,
    paddingVertical:15,
    paddingHorizontal:10,
  }
});
