import React from 'react';
import { StyleSheet, Text, View, StatusBar, YellowBox } from 'react-native';
import {createStackNavigator} from 'react-navigation';
import Form from './components/Form';
import LoginScreen from './screens/LoginScreen';
import SignUpScreen from './screens/SignUpScreen';
import SearchScreen from './screens/SearchScreen';
import SearchResultsScreen from './screens/SearchResultsScreen';
import SettingsScreen from './screens/SettingsScreen';
import ListScreen from './screens/ListScreen';

YellowBox.ignoreWarnings(['Setting a timer']);
const firebase=require('firebase');
const key=require('./config/apiKey').firebaseConfig;
firebase.initializeApp(key);

export default class App extends React.Component {

  render() {
    return (
      <RootStackNav />
    );
  }
}

const RootStackNav=createStackNavigator({
  Login:LoginScreen,
  Signup:SignUpScreen,
  Search:SearchScreen,
  SearchResults:SearchResultsScreen,
  Settings:SettingsScreen,
  MyList: ListScreen
},
{
    initialRouteName: 'Login',
},
);
