import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native';

export default class MovieListElement extends React.Component {

  constructor(props){
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text>Title: {this.props.movieInfo.title}</Text>
          <Text>Status: {this.props.movieInfo.status}</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.updateButton} onPress={this.props.updateMethod}>
            <Text style={{color:'white'}}>Update</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.deleteButton} onPress={this.props.deleteMethod}>
            <Text style={{color:'white'}}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    borderBottomColor: '#cccccc',
    borderBottomWidth: 0.5,
    flex:1,
    flexDirection:'row',
    padding: 5,
  },
  textContainer:{
    flex:1,
    flexDirection:'column',
  },
  buttonContainer:{
    padding:5,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  updateButton:{
    width: 70,
    height: 35,
    padding: 10,
    backgroundColor: 'green',
    borderRadius: 5,
    justifyContent:'center'
  },
  deleteButton:{
    width:60,
    height:35,
    padding:10,
    backgroundColor: 'red',
    borderRadius:5,
    justifyContent:'center'
  },
});
