import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, Platform, ToastAndroid} from 'react-native';

const firebase=require('firebase');
const sha256 = require('js-sha256');

export default class MovieSearchResult extends React.Component {

  constructor(props){
    super(props);

    this.state={
      moviedbID: props.movieInfo.id,
      title: props.movieInfo.original_title,
      overview:props.movieInfo.overview,
      year:props.movieInfo.release_date,
      poster_path:props.movieInfo.poster_path,
    };

  }

  addToList=()=>{
    const userRef=firebase.database().ref(`users/${sha256(firebase.auth().currentUser.email)}/movieList`);
    userRef.push().set({moviedbID: this.state.moviedbID, title: this.state.title, status: "Plan to watch"});
    if(Platform.OS==='android')
      ToastAndroid.show('Added to list', ToastAndroid.LONG);
    }

  render(){
    let button;
    if (this.props.status === "Not added"){
      button=<TouchableOpacity style={styles.addButton} onPress={()=>this.addToList()}>
        <Text style={styles.addButtonText}>Add to List</Text>
      </TouchableOpacity>;
    }
    else {
      button=<TouchableOpacity style={styles.addButton} onPress={()=>console.log("Film was already added")}>
        <Text style={styles.addButtonText}>Added!</Text>
      </TouchableOpacity>;
    }
    return (
      <View style={styles.filmContainer}>

        <View style={styles.posterContainer}>
          <Image source={(this.state.poster_path === null) ? require('../assets/noposter.jpg')
            :{uri:'https://image.tmdb.org/t/p/w500/'+this.state.poster_path}} style={styles.posterStyle}/>
          <Text>Release date:{(this.state.year === "") ? "\nTBA" : this.state.year}</Text>
        {button}
        </View>

        <View style={styles.textContainer}>
          <Text style={styles.title}>{this.state.title}</Text>
          <Text>{(this.state.overview === "") ? "No overview available" : this.state.overview}</Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  filmContainer:{
    flex:1,
    flexDirection:'row',
    paddingHorizontal:15,
    paddingVertical:15,
    borderBottomColor: '#cccccc',
    borderBottomWidth: 1,
  },
  textContainer:{
    flex:1,
  },
  posterContainer:{
    flex:1,
  },
  posterStyle:{
    width:100,
    height:150,
  },
  title:{
    fontFamily:'Roboto',
    fontSize:15,
    fontWeight:'bold'
  },
  addButton:{
    alignItems:'center',
    backgroundColor:'#DDDDDD',
    width:100,
    marginTop:5,
  },
  addButtonText:{
    fontSize:13,
  }
});
