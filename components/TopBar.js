import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class TopBar extends React.Component {

  render(){
    return(
      <View style={styles.navBar}>
        <Text style={styles.topHeader}>{this.props.header}</Text>
      </View>
    );
  }
}

const styles= StyleSheet.create({
  navBar:{
    height:70,
    backgroundColor:"white",
    elevation:3,
    paddingTop:24,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  topHeader:{
    fontSize:20,
    paddingLeft:20,
    paddingTop:10,
  }
});
