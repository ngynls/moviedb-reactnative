import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class BottomBar extends React.Component {

  render(){
    return(
      <View style={styles.bottomNav}>
        <TouchableOpacity style={styles.bottomNavItems} onPress={()=>this.props.navigation.navigate('MyList')}>
          <Icon name='movie' size={25}/>
          <Text>My list</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.bottomNavItems} onPress={()=>this.props.navigation.navigate('Search')}>
          <Icon name='search' size={25}/>
          <Text>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.bottomNavItems} onPress={()=>this.props.navigation.navigate('Settings')}>
          <Icon name='settings' size={25}/>
          <Text>Settings</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles= StyleSheet.create({
  bottomNav:{
    flexDirection:'row',
    justifyContent:'space-evenly',
    backgroundColor: "white",
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
  },
  bottomNavItems:{
    alignItems:'center',
    justifyContent:'center',
  }
});
