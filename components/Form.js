import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ToastAndroid, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const firebase=require('firebase');
const sha256 = require('js-sha256');

export default class Form extends React.Component {
  constructor(props){
    super(props);

    this.state={
      email: '',
      password: '',
      secureTextEntry:true,
      iconName:'eye'
    };
  }

  loginUser=(email,password)=>{

    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(()=>{
      //user is signed in, show platform specific logged in message then redirect to activity screen
      if(Platform.OS==='android')
        ToastAndroid.show('User is logged in!', ToastAndroid.LONG);
      this.redirectToMainActivity();
    })
    .catch((error)=>{
      if(Platform.OS==='android')
        ToastAndroid.show(error.toString(), ToastAndroid.LONG);
      this.redirectToLoginScreen();
    });
  }

  signInUser=(email,password)=>{

    try{

      if(password.length < 6){
        alert("Please enter a password with at least 6 characters");
        return;
      }
      firebase.auth().createUserWithEmailAndPassword(email,password);
      //add user into the database
      const userRef=firebase.database().ref(`users/${sha256(email)}`);
      const currentDate=new Date();
      const currentDateString=new Date(currentDate.toJSON()).toUTCString();
      userRef.set({
          memberSince: currentDateString,
          movieList: "Empty list"
      });
      if(Platform.OS==='android')
        ToastAndroid.show('User is registered!', ToastAndroid.LONG);
    }
    catch(err){
      console.log(err.toString());
    }

  }

  redirectToMainActivity=()=>this.props.navigation.navigate('Search');

  redirectToLoginScreen=()=>this.props.navigation.navigate('Login');

  onEyePress=()=> {
    let iconName= (this.state.secureTextEntry) ? 'eye-off' : 'eye';
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
      iconName: iconName
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)' placeholder='Email' keyboardType='email-address' onChangeText={(email)=>this.setState({email})}/>
        <View style={styles.passwordInputBox}>
          <TextInput style={{flex:1}} underlineColorAndroid='rgba(0,0,0,0)' placeholder='Password' secureTextEntry={this.state.secureTextEntry} onChangeText={(password)=>this.setState({password})}/>
            <TouchableOpacity onPress={()=>this.onEyePress()}>
              <Icon name={this.state.iconName} size={25} />
            </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.loginButton}
          onPress={(this.props.role==='Login') ? ()=> this.loginUser(this.state.email, this.state.password) : ()=>this.signInUser(this.state.email,this.state.password)}>
          <Text style={styles.textBold}> {this.props.role} </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:100,
  },
  inputBox: {
    width:300,
    backgroundColor: '#ffffff',
    color: '#000000',
    paddingVertical:10,
    paddingHorizontal:15,
    marginVertical:10,
    borderRadius:5,
  },
  passwordInputBox:{
    width:300,
    backgroundColor: '#ffffff',
    paddingVertical:10,
    paddingHorizontal:15,
    marginVertical:10,
    borderRadius:5,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  loginButton:{
    width:300,
    backgroundColor:'#aeaeae',
    borderRadius:20,
    alignItems:'center',
    paddingVertical:10,
    paddingHorizontal:10,
    marginVertical:10,
  },
  textBold:{
    color:'#000000',
    fontWeight:'bold',
  },
});
