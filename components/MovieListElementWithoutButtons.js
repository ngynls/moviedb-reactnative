import React from 'react';
import { StyleSheet, Text, View,} from 'react-native';

export default class MovieListElementWithoutButtons extends React.Component {

  constructor(props){
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text>Title: {this.props.movieInfo.title}</Text>
          <Text>Status: {this.props.movieInfo.status}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    borderBottomColor: '#cccccc',
    borderBottomWidth: 0.5,
    flex:1,
    flexDirection:'row',
    padding: 5,
  },
  textContainer:{
    flex:1,
    flexDirection:'column',
  },
});
